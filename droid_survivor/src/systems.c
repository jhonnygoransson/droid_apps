#include "systems.h"

void SYSTEM_Renderable( t_app* app, unsigned int mask, unsigned int renderable_id, unsigned int position_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	droid_quadbatch_bind( app->qb, app->shader );

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_renderable* _renderable = (c_renderable*) droid_get_component( app->game, i, renderable_id );
			c_position* _position     = (c_position*) droid_get_component( app->game, i, position_id );

			droid_mat4x4 _transform;
			droid_mat4x4_identity(_transform);
			droid_mat4x4_translate( _transform, _position->x, _position->y, 0.0f);
			droid_mat4x4_scale_aniso( _transform, _transform, _renderable->scale_x, _renderable->scale_y, 1.0 );

			droid_bind_shader_uniform_matrix4fv( app->shader, "u_transform", (float*) &_transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_camera_transform", (float*) &app->camera->transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_projection", (float*) &app->camera->projection, 0 );
			droid_bind_shader_uniform_i( app->shader, "u_material", _renderable->material );
			droid_bind_shader_uniform_3f( app->shader, "u_color", _renderable->color[0], _renderable->color[1], _renderable->color[2] );

			// DO RENDER HERE
			droid_quadbatch_draw();
		}
	}

	droid_quadbatch_unbind();
}

void SYSTEM_Camera( t_app* app, unsigned int mask, unsigned int camera_id, unsigned int position_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_camera* _camera            = (c_camera*) droid_get_component( app->game, i, camera_id );
			c_position* _camera_position = (c_position*) droid_get_component( app->game, i, position_id );
			c_position* _entity_position = (c_position*) droid_get_component( app->game, _camera->entity, position_id );

			// todo -> support for multi cameras somehow..
			droid_mat4x4 _camera_transform;
			droid_mat4x4_identity(_camera_transform);
			droid_mat4x4_translate( _camera_transform, _entity_position->x, _entity_position->y, 0.0f);
			droid_mat4x4_invert( app->camera->transform, _camera_transform );
		}
	}
}

void SYSTEM_Movement( t_app* app, unsigned int mask, unsigned int movement_id, unsigned int position_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_position* _position = (c_position*) droid_get_component( app->game, i, position_id );
			c_movement* _movement = (c_movement*) droid_get_component( app->game, i, movement_id );

			_position->x += (float) (_movement->vx * app->game->frame_delta);
			_position->y += (float) (_movement->vy * app->game->frame_delta);

			_movement->vx = 0.0f;
			_movement->vy = 0.0f;
		}
	}
}

void SYSTEM_Bullets( t_app* app, unsigned int mask, unsigned int bullet_id, unsigned int movement_id, unsigned int position_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_movement* _movement = (c_movement*) droid_get_component( app->game, i, movement_id );
			c_bullet* _bullet     = (c_bullet*) droid_get_component( app->game, i, bullet_id );

			if ( (app->game->game_time - _bullet->timestamp) > 1.0 )
			{
				DROID_DEBUG("Destroying bullet with index %d", i );
				droid_destroy_entity( app->game, i );
			} 
			else 
			{
				_movement->vx += _bullet->direction_x * _bullet->speed;
				_movement->vy += _bullet->direction_y * _bullet->speed;
			}
		}
	}
}

void SYSTEM_Collidable( t_app* app, unsigned int mask, unsigned int bullet_mask, unsigned int player_mask, unsigned int enemy_mask, unsigned int collidable_id, unsigned int position_id, unsigned int bullet_id, unsigned int enemy_id, unsigned int renderable_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int  i=0;
	unsigned int  j=0;
	unsigned int ci=0;
	unsigned int ccount=0;

	droid_cache* collidable_entities = droid_cache_create( 4, sizeof(unsigned int) );

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask))
		{
			unsigned int* _pair = (unsigned int*) droid_cache_get( collidable_entities, ci++ );
			*_pair = i;
		}
	}

	for(i=0;i< droid_cache_size(collidable_entities); i++ )
	{
		for(j=0;j< droid_cache_size(collidable_entities); j++ )
		{
			c_position*   _position_a, *_position_b;
			c_collidable* _collidable_a, *_collidable_b;

			float x0_a,x1_a,y0_a,y1_a;
			float x0_b,x1_b,y0_b,y1_b;

			unsigned long entity_a = *(entities+i);
			unsigned long entity_b = *(entities+j);

			if ( i == j ) continue;

			_position_a   = (c_position*) droid_get_component( app->game, i, position_id );
			_collidable_a = (c_collidable*) droid_get_component( app->game, i, collidable_id );

			_position_b   = (c_position*) droid_get_component( app->game, j, position_id );
			_collidable_b = (c_collidable*) droid_get_component( app->game, j, collidable_id );

			x0_a = _position_a->x - _collidable_a->width/2.0f;
			x1_a = _position_a->x + _collidable_a->width/2.0f;
			y0_a = _position_a->y - _collidable_a->height/2.0f;
			y1_a = _position_a->y + _collidable_a->height/2.0f;

			x0_b = _position_b->x - _collidable_b->width/2.0f;
			x1_b = _position_b->x + _collidable_b->width/2.0f;
			y0_b = _position_b->y - _collidable_b->height/2.0f;
			y1_b = _position_b->y + _collidable_b->height/2.0f;

			if ( x0_b >= x1_a || 
				 x1_b <= x0_a || 
				 y0_b >= y1_a || 
				 y1_b <= y0_a ) continue;

			if ( DROID_HAS_FLAGS(entity_a,bullet_mask) && DROID_HAS_FLAGS(entity_b,player_mask) ||
				 DROID_HAS_FLAGS(entity_b,bullet_mask) && DROID_HAS_FLAGS(entity_a,player_mask) )
			{
				c_bullet* _bullet        = 0;
				int _bullet_id           = 0;
				int _other_id            = -1;
				unsigned int _other_mask = 0;

				if ( DROID_HAS_FLAGS(entity_a,bullet_mask) )
				{
					_bullet = (c_bullet*) droid_get_component( app->game, i, bullet_id );
					_bullet_id = i;

					_other_id = j;
					_other_mask = entity_b;
				}					
				else
				{
					_bullet     = (c_bullet*) droid_get_component( app->game, j, bullet_id );
					_bullet_id  = j;

					_other_id   = i;
					_other_mask = entity_a;
				}

				if ( _bullet->emitter_id == _other_id )
				{
					DROID_DEBUG("Found a collision between a bullet and its emitter, skipping..");
					continue;
				}	

				DROID_DEBUG("Found a collision between a bullet and an object (ids are %d and %d)", i,j);

				if ( DROID_HAS_FLAGS( _other_mask, enemy_mask ) )
				{
					c_enemy* _enemy = (c_enemy*) droid_get_component( app->game, j, enemy_id );
					c_renderable* _renderable = (c_renderable*) droid_get_component( app->game, j, renderable_id );

					_enemy->health       -= _bullet->damage;
					_renderable->color[0] = _enemy->health / _enemy->max_health;

					if ( _enemy->health <= 0.0f )
						droid_destroy_entity( app->game, _other_id );

					droid_destroy_entity( app->game, _bullet_id );
				}
				else
				{
					droid_destroy_entity( app->game, i );
					droid_destroy_entity( app->game, j );
				}
			}
		}
	}
}

void SYSTEM_Enemy( t_app* app, unsigned int mask, unsigned int enemy_id, unsigned int position_id, unsigned int movement_id )
{
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_movement* _movement        = (c_movement*) droid_get_component( app->game, i, movement_id );
			c_position* _position        = (c_position*) droid_get_component( app->game, i, position_id );
			c_enemy*    _enemy           = (c_enemy*) droid_get_component( app->game, i, enemy_id );
			c_position* _position_target = (c_position*) droid_get_component( app->game, _enemy->following, position_id );

			vec2 epos,tpos,tmp,dir;
			epos[0] = _position->x;
			epos[1] = _position->y;

			tpos[0] = _position_target->x;
			tpos[1] = _position_target->y;

			vec2_sub(tmp, tpos, epos);
			vec2_norm( dir, tmp );
			vec2_scale( dir, dir, _enemy->speed );

			_movement->vx += dir[0] * _enemy->inertia;
			_movement->vy += dir[1] * _enemy->inertia;
		}
	}
}