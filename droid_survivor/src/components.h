#ifndef __COMPONENTS_H__
#define __COMPONENTS_H__

typedef struct 
{
	int material;

	float color[3];
	float scale_x;
	float scale_y;

} c_renderable;

typedef struct 
{
	float x;
	float y;
} c_position;

typedef struct 
{
	double vx;
	double vy;
} c_movement;

typedef struct 
{
	int entity;
} c_camera;

typedef struct 
{
	double timestamp;
	float direction_x;
	float direction_y;
	float speed;
	float damage;

	int emitter_id;

} c_bullet;

typedef struct 
{
	float width;
	float height;
} c_collidable;

typedef struct 
{
	float speed;
	float inertia;
	float health;
	float max_health;

	int following;
} c_enemy;

typedef struct 
{
	int type;
} c_construct;

#endif
