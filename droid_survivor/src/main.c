#include "app.h"

int main(int argc, char const *argv[])
{
	DROID_DEBUG("Creating Zombie Survivor 1.0");
	DROID_DEBUG("-----------------");
	DROID_DEBUG("");

	if ( !app_create() )
	{
		exit(EXIT_FAILURE);
	}

	app_run();

	DROID_DEBUG("Shutting down..");

	return 0;
}