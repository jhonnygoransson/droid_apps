#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "droid.h"

typedef struct 
{
	mat4x4 projection;
	mat4x4 transform;
} t_camera;

#endif