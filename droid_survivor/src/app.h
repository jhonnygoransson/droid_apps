#ifndef __APP_H__
#define __APP_H__

#include "droid.h"
#include "camera.h"
#include "gui.h"

#define NUM_ENTITIES 100

typedef struct 
{
	float interval;
	float timer;
	float emit_position_x;
	float emit_position_y;

	int id;

} t_enemy_emitter;

typedef struct 
{
	float interval;
	float timer;
	int   enabled;
	int   did_emit;
	int   id;
} t_bullet_emitter;

typedef struct
{
	droid_instance*  droid;
	droid_game* 	 game;
	droid_shader*    shader;
	droid_quadbatch* qb;

	t_camera*        camera;
	t_gui*           gui;

	droid_vec2       player_direction;

	int              player_id;
	int              cursor_id;

} t_app;

int app_create();
int app_run();

#endif // __APP_H__