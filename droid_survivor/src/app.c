/* system includes */
#include <malloc.h>
#include <time.h>

/* app includes */
#include "app.h"
#include "systems.h"
#include "components.h"

#define randf(a) ((float)rand()/(float)(RAND_MAX/a))

#define BULLET_SPEED 600.0f
#define ENEMY_SPEED  80.0f
#define PLAYER_SPEED 150.0f

static t_app* app = 0;

/* component IDs */
static unsigned int RENDERABLE_ID = 0;
static unsigned int POSITION_ID   = 0;
static unsigned int CAMERA_ID     = 0;
static unsigned int MOVEMENT_ID   = 0;
static unsigned int BULLET_ID     = 0;
static unsigned int COLLIDABLE_ID = 0;
static unsigned int ENEMY_ID      = 0;
static unsigned int CONSTRUCT_ID  = 0;

/* component masks */
static unsigned int RENDER_MASK    = 0;
static unsigned int CAMERA_MASK    = 0;
static unsigned int MOVEMENT_MASK  = 0;
static unsigned int BULLET_MASK    = 0;
static unsigned int COLLISION_MASK = 0;
static unsigned int ENEMY_MASK     = 0;
static unsigned int CONSTRUCT_MASK = 0;

/* entity emitters */
static t_enemy_emitter enemy_emitter_north;
static t_enemy_emitter enemy_emitter_south;
static t_enemy_emitter enemy_emitter_west;
static t_enemy_emitter enemy_emitter_east;

static t_bullet_emitter bullet_emitter;

int create_player( float x, float y )
{
	int _entity = droid_new_entity( app->game );
	c_renderable* _renderable = 0;
	c_position* _position = 0;
	c_movement* _movement = 0;
	c_collidable* _collidable = 0;

	if (_entity == -1) return -1;

	DROID_DEBUG("Creating player");

	droid_enable_components( app->game, _entity, RENDER_MASK | MOVEMENT_MASK | COLLISION_MASK );

	_renderable = (c_renderable*) droid_get_component( app->game, _entity, RENDERABLE_ID );
	_position   = (c_position*) droid_get_component( app->game, _entity, POSITION_ID );
	_movement   = (c_movement*) droid_get_component( app->game, _entity, MOVEMENT_ID );
	_collidable = (c_collidable*) droid_get_component( app->game, _entity, COLLIDABLE_ID );

	_position->x = x;
	_position->y = y;

	_renderable->material = 0;
	_renderable->scale_x  = 16.0f;
	_renderable->scale_y  = 16.0f;

	_movement->vx = 0.0f;
	_movement->vy = 0.0f;

	_collidable->width  = 16;
	_collidable->height = 16;

	return _entity;
}

int create_enemy( float x, float y, float speed, float inertia, float health )
{
	int _entity = droid_new_entity( app->game );

	c_renderable* _renderable = 0;
	c_position* _position = 0;
	c_movement* _movement = 0;
	c_collidable* _collidable = 0;
	c_enemy* _enemy = 0;

	if (_entity == -1) return -1;

	DROID_DEBUG("Creating enemy");

	droid_enable_components( app->game, _entity, ENEMY_MASK );

	_renderable = (c_renderable*) droid_get_component( app->game, _entity, RENDERABLE_ID );
	_position   = (c_position*) droid_get_component( app->game, _entity, POSITION_ID );
	_movement   = (c_movement*) droid_get_component( app->game, _entity, MOVEMENT_ID );
	_collidable = (c_collidable*) droid_get_component( app->game, _entity, COLLIDABLE_ID );
	_enemy      = (c_enemy*) droid_get_component( app->game, _entity, ENEMY_ID );

	_position->x = x;
	_position->y = y;

	_renderable->material = 1;
	_renderable->scale_x = 16.0f;
	_renderable->scale_y = 16.0f;

	_renderable->color[0] = 1.0f;
	_renderable->color[1] = 0.0f;
	_renderable->color[2] = 0.0f;

	_movement->vx = 0.0f;
	_movement->vy = 0.0f;

	_collidable->width  = 16;
	_collidable->height = 16;

	_enemy->speed      = speed;
	_enemy->inertia    = inertia;
	_enemy->following  = app->player_id;
	_enemy->health     = health;
	_enemy->max_health = health;

	return _entity;
}

int create_camera( int entity )
{
	int _entity           = droid_new_entity( app->game );
	c_camera*   _camera   = 0;
	c_position* _position = 0;

	t_camera* camera = 0;

	if (_entity == -1) return -1;

	DROID_DEBUG("Creating camera");

	droid_enable_components( app->game, _entity, CAMERA_MASK );

	_camera   = (c_camera*) droid_get_component( app->game, _entity, CAMERA_ID );
	_position = (c_position*) droid_get_component( app->game, _entity, POSITION_ID );

	_camera->entity = entity;
	_position->x = 0.0f;
	_position->y = 0.0f;

	camera = (t_camera*) malloc( sizeof(t_camera) );

	// setup projection matrix
	mat4x4_ortho(camera->projection, (float) -app->droid->cfg.window_width/2.0f, (float) app->droid->cfg.window_width/2.0f, (float) -app->droid->cfg.window_height/2.0, (float) app->droid->cfg.window_height/2.0, 0.0f, 1.0f);

	// setup transform matrix
	mat4x4_identity(camera->transform);

	app->camera = camera;

	return _entity;
}

int create_bullet(float x, float y, float direction_x, float direction_y, float speed )
{
	int _entity           = droid_new_entity( app->game );

	c_renderable* _renderable = 0;
	c_position* _position     = 0;
	c_movement* _movement     = 0;
	c_bullet* _bullet         = 0;
	c_collidable* _collidable = 0;

	if (_entity == -1) return -1;

	DROID_DEBUG("Creating bullet with index %d", _entity);

	droid_enable_components( app->game, _entity, BULLET_MASK );

	_renderable = (c_renderable*) droid_get_component( app->game, _entity, RENDERABLE_ID );
	_position   = (c_position*) droid_get_component( app->game, _entity, POSITION_ID );
	_movement   = (c_movement*) droid_get_component( app->game, _entity, MOVEMENT_ID );
	_bullet     = (c_bullet*) droid_get_component( app->game, _entity, BULLET_ID );
	_collidable = (c_collidable*) droid_get_component( app->game, _entity, COLLIDABLE_ID );

	_position->x = x;
	_position->y = y;

	_renderable->material = 2;
	_renderable->scale_x = 8.0f;
	_renderable->scale_y = 8.0f;

	_movement->vx = 0.0f;
	_movement->vy = 0.0f;

	_bullet->timestamp = app->game->game_time;

	_bullet->direction_x = direction_x;
	_bullet->direction_y = direction_y;
	_bullet->emitter_id  = app->player_id;
	_bullet->speed       = speed;
	_bullet->damage      = 1.0f;

	_collidable->width  = 8.0f;
	_collidable->height = 8.0f;

	return _entity;
}

int create_construct()
{
	int _entity           = droid_new_entity( app->game );

	c_renderable* _renderable    = 0;
	c_position* _position        = 0;
	c_collidable* _collidable    = 0;
	c_construct* _construct      = 0;
	c_position* _position_player = 0;

	if (_entity == -1) return -1;

	DROID_DEBUG("Creating construct with id %d", _entity);

	droid_enable_components( app->game, _entity, CONSTRUCT_MASK );

	_renderable      = (c_renderable*) droid_get_component( app->game, _entity, RENDERABLE_ID );
	_position        = (c_position*) droid_get_component( app->game, _entity, POSITION_ID );
	_collidable      = (c_collidable*) droid_get_component( app->game, _entity, COLLIDABLE_ID );
	_construct       = (c_construct*) droid_get_component( app->game, _entity, CONSTRUCT_ID );
	_position_player = (c_position*) droid_get_component( app->game, app->player_id, POSITION_ID );

	_position->x = _position_player->x;
	_position->y = _position_player->y;

	_renderable->material = 3;
	_renderable->scale_x  = 16.0f;
	_renderable->scale_y  = 16.0f;

	return _entity;
}

void set_player_direction( float x, float y )
{
	app->player_direction[0] = x;
	app->player_direction[1] = y;
}

void on_mouse( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_mouse_event* _event_data = 0;

	if (!_msg.eventdata) return;

	_event_data = (droid_input_mouse_event*) _msg.eventdata;

	if ( _event_data->button == 0 && _event_data->type == DROID_MOUSE_PRESSED )
	{
		bullet_emitter.enabled = 1;
	}
	else if ( _event_data->button == 0 && _event_data->type == DROID_MOUSE_RELEASED )
	{
		bullet_emitter.enabled = 0;
		bullet_emitter.timer   = 0;
	}
}

void on_keyboard( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_keyboard_event* _event_data = 0;

	if(!_msg.eventdata) return;

	_event_data = (droid_input_keyboard_event*) _msg.eventdata;

	// exit on esc-press
	if ( _event_data->button == DROID_KEY_ESCAPE && _event_data->type == DROID_KEYBOARD_RELEASED )
	{
		droid_exit();	
	} 
	else if ( _event_data->button == DROID_KEY_SPACE && _event_data->type == DROID_KEYBOARD_RELEASED )
	{
		create_construct();
	}
	else if ( ( _event_data->button == DROID_KEY_A || 
				_event_data->button == DROID_KEY_D || 
				_event_data->button == DROID_KEY_W || 
				_event_data->button == DROID_KEY_S ) && 
				_event_data->type == DROID_KEYBOARD_PRESSED_DOWN )
	{
		c_movement* _movement = (c_movement*) droid_get_component( app->game, app->player_id, MOVEMENT_ID );

		switch( _event_data->button )
		{
			case(DROID_KEY_A): _movement->vx -= PLAYER_SPEED; break;
			case(DROID_KEY_D): _movement->vx += PLAYER_SPEED; break;
			case(DROID_KEY_W): _movement->vy += PLAYER_SPEED; break;
			case(DROID_KEY_S): _movement->vy -= PLAYER_SPEED; break;
		}
	}
}

void emit_objects_from_enemy_emitter( t_enemy_emitter* emitter )
{
	emitter->timer += (float) app->game->frame_delta;

	if ( emitter->timer >= emitter->interval )
	{
		float rhealth  = 1.0f + randf(2.9);
		float rinertia = randf(1.0);
		      rinertia = rinertia < 0.25f ? 0.25f : rinertia;

		DROID_DEBUG("Emitting enemy from %d", emitter->id);

		create_enemy( emitter->emit_position_x, emitter->emit_position_y, ENEMY_SPEED, rinertia, rhealth );

		emitter->timer = 0.0f;
	}
}

void emit_objects_from_bullet_emitter( t_bullet_emitter* emitter )
{
	emitter->timer += (float) app->game->frame_delta;

	if ( emitter->enabled )
	{
		if ( !emitter->did_emit )
		{
			droid_input_mouse_state* state = droid_get_mouse_state();

			c_position* _position = (c_position*) droid_get_component( app->game, app->player_id, POSITION_ID );

			droid_vec2 dir,dir_norm;
			dir[0] = (float) state->position_x - app->droid->cfg.window_width/2.0f;
			dir[1] = (float) -(state->position_y - app->droid->cfg.window_height/2.0f);

			vec2_norm(dir_norm, dir);
			create_bullet( _position->x, _position->y, dir_norm[0], dir_norm[1], BULLET_SPEED );

			emitter->did_emit = 1;
		}

		if ( emitter->timer >= emitter->interval )
		{
			emitter->timer    = 0.0f;
			emitter->did_emit = 0;
		}
	}
}

void emit_objects()
{	
	emit_objects_from_enemy_emitter( &enemy_emitter_north );
	emit_objects_from_enemy_emitter( &enemy_emitter_south );
	emit_objects_from_enemy_emitter( &enemy_emitter_east );
	emit_objects_from_enemy_emitter( &enemy_emitter_west );
	emit_objects_from_bullet_emitter( &bullet_emitter );
}

void emit_enemy_init( t_enemy_emitter* emitter, int id, float interval, float x, float y )
{
	emitter->id              = id;
	emitter->interval        = interval;
	emitter->timer           = 0.0f;
	emitter->emit_position_x = x;
	emitter->emit_position_y = y;
}

void emit_bullet_init( t_bullet_emitter* emitter, int id, float interval )
{
	emitter->id       = id;
	emitter->timer    = 0;
	emitter->interval = interval;
	emitter->enabled  = 0;
	emitter->did_emit = 0;
}

void on_init( droid_context* _ctx, droid_event_msg _msg )
{
	// set seed
	srand((unsigned int)time(NULL));

	droid_add_resource_location("survivor_assets");
	
	// get droid instance
	app->droid = droid_get();

	// create game data
	app->game = droid_create_game();

	// pre-allocate entities at fixed size
	droid_allocate_entities( app->game, NUM_ENTITIES );

	// create shader
	app->shader = droid_load_shader( "shader.vs", "shader.fs" );

	// create quatbatch for drawing renderables
	app->qb     = droid_quadbatch_create_with_uv0();

	droid_quadbatch_begin(app->qb);
	droid_quadbatch_add_centered( app->qb, 0.0, 0.0, 1, 1 );
	droid_quadbatch_end(app->qb);

	// gui
	app->gui = create_gui();

	// create components
	RENDERABLE_ID = droid_add_component( app->game, sizeof(c_renderable) );
	POSITION_ID   = droid_add_component( app->game, sizeof(c_position) );
	CAMERA_ID     = droid_add_component( app->game, sizeof(c_camera) );
	MOVEMENT_ID   = droid_add_component( app->game, sizeof(c_movement) );
	BULLET_ID     = droid_add_component( app->game, sizeof(c_bullet) );
	COLLIDABLE_ID = droid_add_component( app->game, sizeof(c_collidable) );
	ENEMY_ID      = droid_add_component( app->game, sizeof(c_enemy) );
	CONSTRUCT_ID  = droid_add_component( app->game, sizeof(c_construct) );



	// create masks
	RENDER_MASK    = RENDERABLE_ID | POSITION_ID;
	CAMERA_MASK    = POSITION_ID | CAMERA_ID;
	MOVEMENT_MASK  = POSITION_ID | MOVEMENT_ID;
	COLLISION_MASK = COLLIDABLE_ID | POSITION_ID;
	BULLET_MASK    = RENDER_MASK | MOVEMENT_MASK | BULLET_ID | COLLISION_MASK;
	ENEMY_MASK     = RENDER_MASK | MOVEMENT_MASK | COLLISION_MASK | ENEMY_ID;
	CONSTRUCT_MASK = RENDER_MASK | COLLISION_MASK;

	// create entities
	{
		app->player_id = create_player( 0.0f, 0.0f );
		app->player_direction[0] = 1.0f;
		app->player_direction[1] = 0.0f;

		// create camera and assign an entity to look at
		create_camera( app->player_id );
	}

	// initiate emitters
	{
		int emitter_id = 0;

		emit_enemy_init( &enemy_emitter_north, emitter_id++, 2.0f,  0.0f,  256.0f );
		emit_enemy_init( &enemy_emitter_south, emitter_id++, 3.0f,  0.0f, -256.0f );
		emit_enemy_init( &enemy_emitter_east,  emitter_id++, 4.0f,  256.0f, 0.0f );
		emit_enemy_init( &enemy_emitter_west,  emitter_id++, 5.0f, -256.0f, 0.0f );

		emit_bullet_init( &bullet_emitter, emitter_id++, 0.15f );
	}
}

void on_render( droid_context* _ctx, droid_event_msg _msg )
{
	unsigned long* entities = droid_get_entities( app->game );

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	emit_objects();

	// execute systems
	SYSTEM_Bullets( app, BULLET_MASK, BULLET_ID, MOVEMENT_ID, POSITION_ID );

	SYSTEM_Enemy( app, ENEMY_MASK, ENEMY_ID, POSITION_ID, MOVEMENT_ID );

	SYSTEM_Movement( app, MOVEMENT_MASK, MOVEMENT_ID, POSITION_ID );

	SYSTEM_Collidable( app, COLLISION_MASK, BULLET_MASK, RENDER_MASK, ENEMY_MASK, COLLIDABLE_ID, POSITION_ID, BULLET_ID, ENEMY_ID, RENDERABLE_ID );

	SYSTEM_Camera( app, CAMERA_MASK, CAMERA_ID, POSITION_ID );

	SYSTEM_Renderable( app, RENDER_MASK, RENDERABLE_ID, POSITION_ID );

	render_gui( app->gui, app->camera );
}

int app_create()
{
	if ( app ) return 1;

	app = (t_app*) malloc( sizeof(app) );

	app->droid  = 0;
	app->game   = 0;
	app->shader = 0;
	app->camera = 0;
	app->qb     = 0;

	return (app > 0);
}

int app_run()
{
	droid_config cfg;

	cfg.window_width  		= 512;
	cfg.window_height 		= 512;
	cfg.window_fullscreen 	= 0;
	cfg.window_title  		= "Zombie Survivor 1.0";

	if(!droid_create( cfg )) return 0;

	droid_on_event( DROID_EVENT_INIT, &on_init );
	droid_on_event( DROID_EVENT_RENDER, &on_render );
	droid_on_event( DROID_EVENT_INPUT_KEYBOARD, &on_keyboard );
	droid_on_event( DROID_EVENT_INPUT_MOUSE, &on_mouse );

	droid_run();
	droid_destroy();

	destroy_gui( app->gui );

	// hm.. why crash?
	// DROID_FREE_PTR(app);

	return 1;
}
