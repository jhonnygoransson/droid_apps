#ifndef __SYSTEMS_H__
#define __SYSTEMS_H__

#include "app.h"
#include "components.h"

void SYSTEM_Renderable( t_app* app, unsigned int mask, unsigned int renderable_id, unsigned int position_id );
void SYSTEM_Camera( t_app* app, unsigned int mask, unsigned int camera_id, unsigned int position_id );
void SYSTEM_Movement( t_app* app, unsigned int mask, unsigned int movement_id, unsigned int position_id );
void SYSTEM_Bullets( t_app* app, unsigned int mask, unsigned int bullet_id, unsigned int movement_id, unsigned int position_id );
void SYSTEM_Collidable( t_app* app, unsigned int mask, unsigned int bullet_mask, unsigned int player_mask, unsigned int enemy_mask, unsigned int collidable_id, unsigned int position_id, unsigned int bullet_id, unsigned int enemy_id, unsigned int renderable_id );

void SYSTEM_Enemy( t_app* app, unsigned int mask, unsigned int enemy_id, unsigned int position_id, unsigned int movement_id );

#endif