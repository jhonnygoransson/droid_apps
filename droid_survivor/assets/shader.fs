varying vec2 v_uv;

uniform int  u_material;
uniform vec3 u_color;

void main()
{
	vec3 color = vec3(1.0);

	if ( u_material == 1 ) {
		color = u_color;
	}
	else if ( u_material == 2 ) color = vec3( 1.0, 1.0, 0.0 );
	else if ( u_material == 3 ) color = vec3( 0.0, 1.0, 0.0 );

	gl_FragColor = vec4( color, 1.0 );
}