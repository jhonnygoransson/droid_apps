local buildroot = "../" .. buildroot

project "droid_survivor"
	location ( buildroot .. "/projects/droid_apps/droid_survivor" )
	targetdir ( buildroot .. "/bin/" )
	debugdir ( buildroot .. "/bin/" )

	kind "ConsoleApp"
	files { "src/*.c", "src/*.h" }

	defines { "DROID_BUILD_SHARED", "GLEW_STATIC" }

	includedirs { 
		"src", 
		includeroot .. "src", 
		includeroot .. "libs/GLFW/include/",
		includeroot .. "libs/GLEW/include/",
		includeroot .. "libs/linmath.h/",
		includeroot .. "src/droid_core/"
	}

	libdirs { buildroot .. "/lib" }

	links { "droid_lib_shared", "GLEW", "glfw3" }

	if os.get() == "windows" then
		postbuildcommands { "echo d | xcopy /C /E /I /y ..\\..\\..\\..\\apps\\droid_survivor\\assets ..\\..\\..\\bin\\survivor_assets "  }
	end