varying vec2 v_uv;

uniform sampler2D u_tex0;
uniform float u_blob_threshold;

void main()
{
	vec4 sample  = texture2D( u_tex0, v_uv );


	if( sample.a < (1.0-u_blob_threshold) && sample.a > (1.0-u_blob_threshold-0.1) ) 
	{
		gl_FragColor = vec4( 0.0, 0.0, 0.0, 1.0 );
	} 
	else if ( sample.a > u_blob_threshold )
	{
		gl_FragColor = vec4(1.0);
	} 
	else discard;
}