attribute vec3 a_position;
attribute vec2 a_uv0;

varying vec2 v_uv;

void main()
{
	v_uv                = a_uv0;	
	gl_Position         = vec4(a_position,1.0);
}