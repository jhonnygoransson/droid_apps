attribute vec3 a_position;
attribute vec2 a_uv0;

uniform mat4 u_transform;
uniform mat4 u_projection;

varying vec2 v_uv;

void main()
{
	v_uv                = a_uv0;	
	vec4 world_position = u_transform * vec4(a_position.xy, 0.0,1.0);
	gl_Position         = u_projection * vec4( world_position.xyz, 1.0);
}