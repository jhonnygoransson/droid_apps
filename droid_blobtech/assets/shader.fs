varying vec2 v_uv;

uniform sampler2D u_tex0;

void main()
{
	vec4 sample  = texture2D( u_tex0, v_uv );
	gl_FragColor = sample;
}