#include <malloc.h>

#include "app.h"


typedef struct
{
	float x;
	float y;
	float w;
	float h;
} t_blob;

void add_blob( float x, float y, float w, float h )
{
	t_blob* blob = droid_cache_get(app->blob_data, droid_cache_size(app->blob_data));

	blob->x = x;
	blob->y = y;
	blob->w = w;
	blob->h = h;

	DROID_DEBUG("Blob added at %f,%f",x,y);
}

void clear_blobs()
{
	droid_cache_resize( app->blob_data, 1 );
	app->blob_data->end_ptr = 0;
}

void commit_blobs()
{
	if ( droid_cache_size(app->blob_data) != app->blobs_commited_count )
	{
		unsigned int i = 0;

		droid_quadbatch_begin(app->blobs_qb);
		for(i;i<droid_cache_size(app->blob_data);i++)
		{
			t_blob* blob = droid_cache_get(app->blob_data,i);
			droid_quadbatch_add_centered( app->blobs_qb, blob->x, blob->y, blob->w, blob->h );
		}
		droid_quadbatch_end(app->blobs_qb);

		app->blobs_commited_count = droid_cache_size(app->blob_data);
	}
}

void draw_blobs()
{
	unsigned int i = 0;

	if ( app->blobs_qb->count == 0 ) return;

	droid_quadbatch_bind( app->blobs_qb, app->shader );
	droid_bind_shader_uniform_matrix4fv( app->shader, "u_projection", (float*) &app->camera->projection, 0 );

	droid_bind_texture( app->blob_mask, 0 );
	droid_bind_shader_uniform_i( app->shader, "u_tex0", 0 );

	{
		droid_mat4x4 _transform;
		droid_mat4x4_identity(_transform);
		droid_bind_shader_uniform_matrix4fv( app->shader, "u_transform", (float*) &_transform, 0 );
		droid_quadbatch_draw();
	}

	/*
	for(i;i<droid_cache_size(app->blob_data);i++)
	{
		t_blob* blob = droid_cache_get(app->blob_data,i);

		droid_mat4x4 _transform;
		droid_mat4x4_identity(_transform);
		droid_mat4x4_translate( _transform, blob->x, blob->y, 0.0f);
		
		droid_bind_shader_uniform_matrix4fv( app->shader, "u_transform", (float*) &_transform, 0 );
		droid_quadbatch_draw();
	}
	*/

	droid_quadbatch_unbind();
}

void on_keyboard( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_keyboard_event* _event_data = 0;

	if(!_msg.eventdata) return;

	_event_data = (droid_input_keyboard_event*) _msg.eventdata;

	// exit on esc-press
	if ( _event_data->button == DROID_KEY_ESCAPE && _event_data->type == DROID_KEYBOARD_RELEASED )
	{
		droid_exit();	
	} 
}

void on_mouse( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_mouse_event* _event_data = 0;
	droid_input_mouse_state* _mouse_state = 0;

	if (!_msg.eventdata) return;

	_event_data = (droid_input_mouse_event*) _msg.eventdata;
	_mouse_state = droid_get_mouse_state();

	if ( _event_data->button == 0 && _event_data->type == DROID_MOUSE_PRESSED_DOWN )
	{
		float x = (float) _mouse_state->position_x - app->droid->cfg.window_width/2.0f;
		float y = (float) -(_mouse_state->position_y - app->droid->cfg.window_height/2.0f);

		add_blob( x,y, 64.0f,64.0f );
	}
	else if ( _event_data->button == 1 && _event_data->type == DROID_MOUSE_PRESSED )
	{
		clear_blobs();
	}
}

void on_init( droid_context* _ctx, droid_event_msg _msg )
{
	droid_add_resource_location("blobtech_assets");

	// get droid instance
	app->droid = droid_get();

	// load assets : shader
	app->shader = droid_load_shader( "shader.vs", "shader.fs" );
	app->shader_fs = droid_load_shader( "shader_fs.vs", "shader_fs.fs" );

	// create game camera
	app->camera = droid_create_camera_orthographic( 
		(float) -app->droid->cfg.window_width/2.0f, 
		(float) app->droid->cfg.window_width/2.0f, 
		(float) -app->droid->cfg.window_height/2.0f, 
		(float) app->droid->cfg.window_height/2.0f, 
		0.0f, 1.0f );

	{
		droid_texture_params_default(texture_parameters);
		app->blob_mask = droid_load_texture( "blob_mask.png", texture_parameters );
	}

	app->blobs_qb = droid_quadbatch_create_with_uv0();
	app->fs_qb = droid_quadbatch_create_with_uv0();
	app->blob_data = droid_cache_create( 8, sizeof(t_blob));
	app->blobs_commited_count = 0;

	/*
	droid_quadbatch_begin(app->blobs_qb);
	droid_quadbatch_add_centered( app->blobs_qb, 0.0f, 0.0f, 128.0f, 128.0f );
	droid_quadbatch_end(app->blobs_qb);
	*/

	droid_quadbatch_begin(app->fs_qb);
	droid_quadbatch_add_centered( app->fs_qb, 0.0f, 0.0f, 2.0f, 2.0f );
	droid_quadbatch_end(app->fs_qb);

	{
		droid_texture_params_default(_fbo_color0_params);
		_fbo_color0_params.width      = app->droid->cfg.window_width;
		_fbo_color0_params.height     = app->droid->cfg.window_height;
		app->blob_rt = droid_create_texture( 0, _fbo_color0_params );
	}

	{
		droid_render_buffer_params _fbo_depth_params;
		_fbo_depth_params.internal_format = GL_DEPTH_COMPONENT;
	   	_fbo_depth_params.width           = app->droid->cfg.window_width;
	   	_fbo_depth_params.height          = app->droid->cfg.window_height;
	   	app->blob_depth      = droid_create_render_buffer(_fbo_depth_params);
	}
	
	app->blob_fbo = droid_create_framebuffer();

	droid_framebuffer_attach_color_texture( app->blob_fbo, app->blob_rt, 0 );
	droid_framebuffer_attach_depth_buffer( app->blob_fbo, app->blob_depth );
}

void on_render( droid_context* _ctx, droid_event_msg _msg )
{
	commit_blobs();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	

	droid_bind_framebuffer( app->blob_fbo );
	glClearColor( 0.0f,0.0f,0.0f, 0.0f );

	glEnable (GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glClear( GL_COLOR_BUFFER_BIT );

	draw_blobs();
	droid_bind_framebuffer( 0 );

	glDisable(GL_BLEND);
	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	droid_quadbatch_bind( app->fs_qb, app->shader_fs );
	droid_bind_texture( app->blob_rt, 0 );
	droid_bind_shader_uniform_i( app->shader_fs, "u_tex0", 0 );
	droid_bind_shader_uniform_f( app->shader_fs, "u_blob_threshold", 0.5f );
	droid_quadbatch_draw();

	droid_quadbatch_unbind();

	// droid_run_systems( app->game );
}

int app_create()
{
	if ( app ) return 1;

	app = (t_app*) malloc( sizeof(app) );

	app->droid  = 0;
	app->game   = 0;

	return (app > 0);
}

int app_run()
{
	droid_config cfg;

	cfg.window_width  		= 512;
	cfg.window_height 		= 512;
	cfg.window_fullscreen 	= 0;
	cfg.window_title  		= "Blobtech 1.0";

	if(!droid_create( cfg )) return 0;

	app->game = droid_create_game();

	droid_on_event( DROID_EVENT_INIT, &on_init );
	droid_on_event( DROID_EVENT_RENDER, &on_render );
	droid_on_event( DROID_EVENT_INPUT_KEYBOARD, &on_keyboard );
	droid_on_event( DROID_EVENT_INPUT_MOUSE, &on_mouse );

	droid_run();
	// droid_destroy();

	// DROID_FREE_PTR(app);

	return 1;
}
