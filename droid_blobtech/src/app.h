#ifndef __APP_H__
#define __APP_H__

#include "droid.h"

typedef struct
{
	droid_instance*  droid;
	droid_game* 	 game;

	droid_shader*    shader;
	droid_shader*    shader_fs;
	droid_camera*    camera;

	droid_quadbatch* blobs_qb;
	droid_quadbatch* fs_qb;

	droid_texture*       blob_mask;
	droid_texture*       blob_rt;
	droid_render_buffer* blob_depth;
	droid_framebuffer*   blob_fbo;

	droid_cache* blob_data;
	unsigned int blobs_commited_count;
} t_app;

int app_create();
int app_run();

t_app* app;

#endif // __APP_H__