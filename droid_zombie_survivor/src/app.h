#ifndef __APP_H__
#define __APP_H__

#include "grid.h"
#include "droid.h"

#define NUM_ENTITIES 100

typedef struct
{
	droid_instance*  droid;
	droid_game* 	 game;

	droid_shader*    shader;
	droid_camera*    camera;
	droid_quadbatch* qb;

	droid_entity*    player;

	t_grid* grid;

} t_app;

int app_create();
int app_run();

t_app* app;

unsigned int RENDERABLE_ID;
unsigned int POSITION_ID;
unsigned int MOVABLE_ID;
unsigned int CAMERA_ID;
unsigned int EMITTER_ID;
unsigned int BULLET_ID;
unsigned int COLLIDER_ID;

#endif // __APP_H__