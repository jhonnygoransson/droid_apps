#include <malloc.h>

#include "app.h"
#include "components.h"

extern void SYSTEM_Render( droid_game*, droid_system* );
extern void SYSTEM_Camera( droid_game*, droid_system* );
extern void SYSTEM_Movable( droid_game*, droid_system* );
extern void SYSTEM_Emitter( droid_game*, droid_system* );
extern void SYSTEM_Bullet( droid_game*, droid_system* );
extern void SYSTEM_Collider( droid_game*, droid_system* );


void handler_bullet( droid_game* _game, droid_entity* _entity, droid_entity_msg* _msg )
{
	if ( _msg->type == DROID_ENTITY_MSG_DESTROY_BULLET )
	{
		droid_destroy_entity( _game, _entity );
	}
	else if ( _msg->type == DROID_ENTITY_MSG_COLLISION )
	{
		droid_entity* _other      = _msg->userdata;
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _entity, RENDERABLE_ID );
		c_bullet* _bullet         = (c_bullet*) droid_get_component( _game, _entity, BULLET_ID );

		if ( _bullet->emitter != _other && !(DROID_HAS_FLAGS(_other->components,BULLET_ID)))
		{
			_renderable->material = 2;
			DROID_DEBUG("Bullet hit entity %d", _other->id);
		}
	}
}

droid_entity* create_bullet( droid_game* _game, float _x, float _y, float _vx, float _vy, float _speed, droid_entity* _emitter )
{
	droid_entity* _entity = droid_new_entity( _game, handler_bullet );

	if (!_entity) {
		DROID_DEBUG("Unable to create bullet");
		return 0;
	}

	droid_enable_components( _game, _entity, RENDERABLE_ID | POSITION_ID | MOVABLE_ID | BULLET_ID | COLLIDER_ID );
	{
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _entity, RENDERABLE_ID );
		c_position* _position     = (c_position*) droid_get_component( _game, _entity, POSITION_ID );
		c_movable* _movable       = (c_movable*) droid_get_component( _game, _entity, MOVABLE_ID );
		c_bullet* _bullet         = (c_bullet*) droid_get_component( _game, _entity, BULLET_ID );
		c_collider* _collider     = (c_collider*) droid_get_component( _game, _entity, COLLIDER_ID );

		_position->x = _x;
		_position->y = _y;

		_renderable->sx       = 8.0f;
		_renderable->sy       = 8.0f;
		_renderable->material = 1;

		_movable->vx = 0.0f;
		_movable->vy = 0.0f;
		_movable->vx_const = _vx * _speed;
		_movable->vy_const = _vy * _speed;

		_bullet->timer    = 0.0f;
		_bullet->duration = 2.0f;
		_bullet->emitter  = _emitter;

		_collider->type = DROID_COLLIDER_AABB;
		_collider->volume.aabb.min[0] = -_renderable->sx * 0.5f;
		_collider->volume.aabb.min[1] = -_renderable->sy * 0.5f;
		_collider->volume.aabb.max[0] =  _renderable->sx * 0.5f;
		_collider->volume.aabb.max[1] =  _renderable->sy * 0.5f;
	}

	return _entity;
}

void handler_player( droid_game* _game, droid_entity* _entity, droid_entity_msg* _msg )
{
	/*
	DROID_DEBUG( "Entity %d received msg type %d", _entity->id, _msg->type );

	if ( _msg->type == DROID_ENTITY_MSG_COMPONENT_ADDED )
	{
		unsigned int _c = *((unsigned int*) _msg->userdata);

		DROID_DEBUG( "  Added Component %d", _c);
	}

	else if ( _msg->type == DROID_ENTITY_MSG_COMPONENT_REMOVED )
	{
		unsigned int _c = *((unsigned int*) _msg->userdata);

		DROID_DEBUG( "  Removed Component %d", _c);
	}
	*/

	if ( _msg->type == DROID_ENTITY_MSG_EMIT_BULLET_START )
	{
		c_emitter* _emitter = (c_emitter*) droid_get_component( _game, _entity, EMITTER_ID );
		_emitter->enabled   = 1;
		_emitter->timer     = _emitter->interval;
	}
	else if ( _msg->type == DROID_ENTITY_MSG_EMIT_BULLET_STOP )
	{
		c_emitter* _emitter = (c_emitter*) droid_get_component( _game, _entity, EMITTER_ID );
		_emitter->enabled   = 0;
	}
	else if ( _msg->type == DROID_ENTITY_MSG_EMIT )
	{
		droid_input_mouse_state* state = droid_get_mouse_state();

		c_position* _position = (c_position*) droid_get_component( _game, _entity, POSITION_ID );

		droid_vec2 dir,dir_norm;
		dir[0] = (float) state->position_x - app->droid->cfg.window_width/2.0f;
		dir[1] = (float) -(state->position_y - app->droid->cfg.window_height/2.0f);

		vec2_norm(dir_norm, dir);
		create_bullet( _game, _position->x, _position->y, dir_norm[0], dir_norm[1], 500.0f, _entity );
	}
	else if ( _msg->type == DROID_ENTITY_MSG_COLLISION )
	{
		droid_entity* _other      = _msg->userdata;
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _entity, RENDERABLE_ID );

		if ( DROID_HAS_FLAGS(_other->components, BULLET_ID) )
		{
			c_bullet* _bullet = (c_bullet*) droid_get_component( _game, _other, BULLET_ID );

			if ( _bullet->emitter != _entity )
			{
				DROID_DEBUG("Player got hit!");
			}
		}
	}
}

droid_entity* create_player( droid_game* _game, float _x, float _y )
{
	droid_entity* _player = droid_new_entity( _game, handler_player );

	if (!_player) {
		DROID_DEBUG("Unable to create player");
		return 0;
	}

	droid_enable_components( _game, _player, RENDERABLE_ID | POSITION_ID | MOVABLE_ID | EMITTER_ID | COLLIDER_ID );
	{
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _player, RENDERABLE_ID );
		c_position* _position     = (c_position*) droid_get_component( _game, _player, POSITION_ID );
		c_movable* _movable       = (c_movable*) droid_get_component( _game, _player, MOVABLE_ID );
		c_emitter* _emitter       = (c_emitter*) droid_get_component( _game, _player, EMITTER_ID );
		c_collider* _collider     = (c_collider*) droid_get_component( _game, _player, COLLIDER_ID );

		_position->x = _x;
		_position->y = _y;

		_movable->vx    = 0.0f;
		_movable->vy    = 0.0f;
		_movable->speed = 175.0f;
		_movable->vx_const = 0.0f;
		_movable->vy_const = 0.0f;

		_renderable->sx = 32.0f;
		_renderable->sy = 32.0f;

		_emitter->type      = 1;
		_emitter->timer     = 0.0f;
		_emitter->interval  = 0.25f;
		_emitter->enabled   = 0;

		_collider->type = DROID_COLLIDER_AABB;
		_collider->volume.aabb.min[0] = -_renderable->sx * 0.5f;
		_collider->volume.aabb.min[1] = -_renderable->sy * 0.5f;
		_collider->volume.aabb.max[0] =  _renderable->sx * 0.5f;
		_collider->volume.aabb.max[1] =  _renderable->sy * 0.5f;
	}

	return _player;
}

droid_entity* create_camera( droid_game* _game, droid_camera* _droid_camera, droid_entity* _lookat )
{
	droid_entity* _entity = droid_new_entity( _game, 0 );

	if (!_entity) {
		DROID_DEBUG("Unable to create camera");
		return 0;
	}

	droid_enable_components( _game, _entity, CAMERA_ID );
	{
		c_camera* _camera = (c_camera*) droid_get_component( _game, _entity, CAMERA_ID );

		_camera->camera = _droid_camera;
		_camera->lookat = _lookat;
	}

	return _entity;
}

void handler_enemy( droid_game* _game, droid_entity* _entity, droid_entity_msg* _msg )
{

}

void create_enemy( droid_game* _game, float _x, float _y )
{
	droid_entity* _enemy = droid_new_entity( _game, handler_enemy );

	if (!_enemy) {
		DROID_DEBUG("Unable to create enemy");
	}

	droid_enable_components( _game, _enemy, RENDERABLE_ID | POSITION_ID | MOVABLE_ID | COLLIDER_ID );
	{
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _enemy, RENDERABLE_ID );
		c_position* _position     = (c_position*) droid_get_component( _game, _enemy, POSITION_ID );
		c_movable* _movable       = (c_movable*) droid_get_component( _game, _enemy, MOVABLE_ID );
		c_collider* _collider     = (c_collider*) droid_get_component( _game, _enemy, COLLIDER_ID );

		_position->x = _x;
		_position->y = _y;

		_movable->vx = 0.0f;
		_movable->vy = 0.0f;
		_movable->vx_const = 0.0f;
		_movable->vy_const = 0.0f;

		_renderable->sx = 16.0f;
		_renderable->sy = 16.0f;

		_collider->type = DROID_COLLIDER_AABB;
		_collider->volume.aabb.min[0] = -_renderable->sx * 0.5f;
		_collider->volume.aabb.min[1] = -_renderable->sy * 0.5f;
		_collider->volume.aabb.max[0] =  _renderable->sx * 0.5f;
		_collider->volume.aabb.max[1] =  _renderable->sy * 0.5f;
	}
}

void on_mouse( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_mouse_event* _event_data = 0;

	if (!_msg.eventdata) return;

	_event_data = (droid_input_mouse_event*) _msg.eventdata;

	if ( _event_data->button == 0 && _event_data->type == DROID_MOUSE_PRESSED )
	{
		droid_entity_msg _msg;
		_msg.type     = DROID_ENTITY_MSG_EMIT_BULLET_START;

		droid_send_message_to_entity( app->game, app->player, &_msg );
	}
	else if ( _event_data->button == 0 && _event_data->type == DROID_MOUSE_RELEASED )
	{
		droid_entity_msg _msg;
		_msg.type = DROID_ENTITY_MSG_EMIT_BULLET_STOP;

		droid_send_message_to_entity( app->game, app->player, &_msg );
	}
}

void on_keyboard( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_keyboard_event* _event_data = 0;

	if(!_msg.eventdata) return;

	_event_data = (droid_input_keyboard_event*) _msg.eventdata;

	// exit on esc-press
	if ( _event_data->button == DROID_KEY_ESCAPE && _event_data->type == DROID_KEYBOARD_RELEASED )
	{
		droid_exit();	
	} 
	else if ( ( _event_data->button == DROID_KEY_A || 
				_event_data->button == DROID_KEY_D || 
				_event_data->button == DROID_KEY_W || 
				_event_data->button == DROID_KEY_S ) && 
				_event_data->type == DROID_KEYBOARD_PRESSED_DOWN )
	{
		c_movable* _movement = (c_movable*) droid_get_component( app->game, app->player, MOVABLE_ID );

		switch( _event_data->button )
		{
			case(DROID_KEY_A): _movement->vx -= _movement->speed; break;
			case(DROID_KEY_D): _movement->vx += _movement->speed; break;
			case(DROID_KEY_W): _movement->vy += _movement->speed; break;
			case(DROID_KEY_S): _movement->vy -= _movement->speed; break;
		}
	}
}

void on_init( droid_context* _ctx, droid_event_msg _msg )
{
	t_grid* _grid = 0;

	droid_add_resource_location("zombie_survivor_assets");

	// get droid instance
	app->droid = droid_get();

	app->grid = grid_create( 4, 4, app->droid->cfg.window_width, app->droid->cfg.window_height );

	// load assets : shader
	app->shader = droid_load_shader( "shader.vs", "shader.fs" );

	app->qb     = droid_quadbatch_create_with_uv0();

	droid_quadbatch_begin(app->qb);
	droid_quadbatch_add_centered( app->qb, 0.0, 0.0, 1, 1 );
	droid_quadbatch_end(app->qb);

	// create game camera
	app->camera = droid_create_camera_orthographic( 
		(float) -app->droid->cfg.window_width/2.0f, 
		(float) app->droid->cfg.window_width/2.0f, 
		(float) -app->droid->cfg.window_height/2.0f, 
		(float) app->droid->cfg.window_height/2.0f, 
		0.0f, 1.0f );

	// add components
	RENDERABLE_ID = droid_add_component( app->game, sizeof(c_renderable) );
	POSITION_ID   = droid_add_component( app->game, sizeof(c_position) );
	MOVABLE_ID    = droid_add_component( app->game, sizeof(c_movable) );
	EMITTER_ID    = droid_add_component( app->game, sizeof(c_emitter) );
	CAMERA_ID     = droid_add_component( app->game, sizeof(c_camera) );
	BULLET_ID     = droid_add_component( app->game, sizeof(c_bullet) );
	COLLIDER_ID   = droid_add_component( app->game, sizeof(c_collider) );

	// add systems
	droid_add_system( app->game, SYSTEM_Bullet, BULLET_ID );
	droid_add_system( app->game, SYSTEM_Emitter, EMITTER_ID );
	droid_add_system( app->game, SYSTEM_Collider, POSITION_ID | COLLIDER_ID );
	droid_add_system( app->game, SYSTEM_Movable, POSITION_ID | MOVABLE_ID );
	droid_add_system( app->game, SYSTEM_Camera, CAMERA_ID );
	droid_add_system( app->game, SYSTEM_Render, RENDERABLE_ID | POSITION_ID );

	// create entities
	droid_allocate_entities( app->game, NUM_ENTITIES );
	{
		droid_entity* _player = create_player( app->game, 0.0f, 0.0f );
		droid_entity* _camera = create_camera( app->game, app->camera, _player );

		create_enemy( app->game, -128.0f, -128.0f );
		create_enemy( app->game,  128.0f, -128.0f );
		create_enemy( app->game, -128.0f,  128.0f );
		create_enemy( app->game,  128.0f,  128.0f );

		app->player = _player;
	}
}

void on_render( droid_context* _ctx, droid_event_msg _msg )
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	droid_run_systems( app->game );
}

int app_create()
{
	if ( app ) return 1;

	app = (t_app*) malloc( sizeof(t_app) );

	app->droid  = 0;
	app->game   = 0;
	app->shader = 0;
	app->camera = 0;
	app->qb     = 0;
	app->grid   = 0;

	return (app > 0);
}

int app_run()
{
	droid_config cfg;

	cfg.window_width  		= 512;
	cfg.window_height 		= 512;
	cfg.window_fullscreen 	= 0;
	cfg.window_title  		= "Zombie Survivor 1.0";

	if(!droid_create( cfg )) return 0;

	app->game = droid_create_game();

	droid_on_event( DROID_EVENT_INIT, &on_init );
	droid_on_event( DROID_EVENT_RENDER, &on_render );
	droid_on_event( DROID_EVENT_INPUT_KEYBOARD, &on_keyboard );
	droid_on_event( DROID_EVENT_INPUT_MOUSE, &on_mouse );

	droid_run();
	droid_destroy();

	/* TODO CLEANUP */

	// DROID_FREE_PTR(app);

	return 1;
}
