#ifndef __COMPONENTS_H__
#define __COMPONENTS_H__

#include "droid_common.h"
#include "droid_game/droid_entity.h"

#define DROID_ENTITY_MSG_EMIT_BULLET_START DROID_ENTITY_MSG_COMPONENT_LAST + 1
#define DROID_ENTITY_MSG_EMIT_BULLET_STOP  DROID_ENTITY_MSG_COMPONENT_LAST + 2
#define DROID_ENTITY_MSG_EMIT              DROID_ENTITY_MSG_COMPONENT_LAST + 3

#define DROID_ENTITY_MSG_DESTROY_BULLET    DROID_ENTITY_MSG_COMPONENT_LAST + 4
#define DROID_ENTITY_MSG_COLLISION         DROID_ENTITY_MSG_COMPONENT_LAST + 5

typedef struct 
{
	int   material;
	float color[3];
	float sx;
	float sy;
} c_renderable;

typedef struct 
{
	float x;
	float y;
} c_position;

typedef struct 
{
	float vx;
	float vy;
	float vx_const;
	float vy_const;
	float speed;
} c_movable;

typedef struct 
{
	float health;
} c_health;

typedef struct 
{
	float damage;
} c_damage;

typedef struct 
{
	droid_entity* lookat;
	droid_camera* camera;
} c_camera;

typedef struct 
{
	float timer;
	float duration;
	droid_entity* emitter;
} c_bullet;

#define DROID_EMITTER_TYPE_NONE   0
#define DROID_EMITTER_TYPE_BULLET 1

typedef struct 
{
	int type;
	int enabled;

	float interval;
	float timer;

} c_emitter;

#define DROID_COLLIDER_NONE   0
#define DROID_COLLIDER_AABB   1
#define DROID_COLLIDER_SPHERE 2

typedef union 
{
	struct {
		float min[2];
		float max[2];
	} aabb;

	struct {
		float radius;
		float center[2];
	} sphere;
} c_collider_volume;

typedef struct 
{
	int type;
	c_collider_volume volume;
} c_collider;

#endif
