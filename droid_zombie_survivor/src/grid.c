#include <malloc.h>
#include <assert.h>

#include "grid.h"

t_grid* grid_create( int _cw, int _ch, float _width, float _height )
{
	t_grid* _grid = (t_grid*) malloc( sizeof(t_grid) );
	assert( _grid );

	_grid->cells = (t_cell*) malloc( sizeof(t_cell) * _cw * _ch );

	_grid->c_width  = _cw;
	_grid->c_height = _ch;
	_grid->width    = _width;
	_grid->height   = _height;

	return _grid;
}

void grid_destroy( t_grid* _grid )
{
	assert(_grid);
	DROID_FREE_PTR(_grid->cells);
	DROID_FREE_PTR(_grid);
}

t_cell* grid_get_cell( t_grid* _grid, int _cx, int _cy )
{
	t_cell* _cell = 0;
	assert(_grid && _grid->cells);

	if ( !grid_in_bounds(_grid, _cx, _cy) ) return 0;

	return &_grid->cells[ _grid->c_height * _cy + _cx ];
}

int grid_in_bounds( t_grid* _grid, int _cx, int _cy )
{
	return _cx >= 0 && _cx < _grid->c_width && _cy >= 0 && _cy < _grid->c_height;
}

int grid_passable( t_grid* _grid, int _cx, int _cy )
{
	t_cell* _cell = grid_get_cell( _grid, _cx, _cy );

	return _cell && _cell->passable;
}

t_cell** grid_get_neighbours( t_grid* _grid, int _cx, int _cy )
{
	assert( _grid && _grid->cells );
	{
		int _i = 0;
		int _x = -1;
		int _y = -1;

		for(_i;_i<8;_i++)
		{
			_grid->neighbour_list[_i] = 0;
		}

		_i = 0;

		for(_y;_y<=1;_y++)
		for(_x;_x<=1;_x++)
		{
			t_cell* _cell = grid_get_cell( _grid, _x, _y );

			if ( _cell->passable )
				_grid->neighbour_list[ _i ] = _cell;
		}
	}

	return _grid->neighbour_list;
}
