#include <assert.h>

#include "app.h"
#include "components.h"

void SYSTEM_Render( droid_game* _game, droid_system* _system )
{
	int i=0;
	droid_entity* entities = droid_get_entities( _game );

	droid_quadbatch_bind( app->qb, app->shader );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _entity, RENDERABLE_ID );
			c_position* _position     = (c_position*) droid_get_component( _game, _entity, POSITION_ID );

			droid_mat4x4 _transform;
			droid_mat4x4_identity(_transform);
			droid_mat4x4_translate( _transform, _position->x, _position->y, 0.0f);
			droid_mat4x4_scale_aniso( _transform, _transform, _renderable->sx, _renderable->sy, 1.0 );

			droid_bind_shader_uniform_matrix4fv( app->shader, "u_transform", (float*) &_transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_camera_transform", (float*) &app->camera->transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_projection", (float*) &app->camera->projection, 0 );
			droid_bind_shader_uniform_i( app->shader, "u_material", _renderable->material );
			droid_bind_shader_uniform_3f( app->shader, "u_color", _renderable->color[0], _renderable->color[1], _renderable->color[2] );

			// DO RENDER 
			droid_quadbatch_draw();
		}
	}

	droid_quadbatch_unbind();
}

void SYSTEM_Camera( droid_game* _game, droid_system* _system )
{
	int i=0;
	droid_entity* entities = droid_get_entities( _game );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_camera*   _camera = (c_camera*) droid_get_component( _game, _entity, CAMERA_ID );
			c_position* _lookat = (c_position*) droid_get_component( _game, _camera->lookat, POSITION_ID );

			droid_mat4x4 _camera_transform;
			droid_mat4x4_identity(_camera_transform);
			droid_mat4x4_translate( _camera_transform, _lookat->x, _lookat->y, 0.0f);
			droid_mat4x4_invert( _camera->camera->transform, _camera_transform );
		}
	}
}

void SYSTEM_Movable( droid_game* _game, droid_system* _system )
{
	int i=0;
	droid_entity* entities = droid_get_entities( _game );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_movable* _movable   = (c_movable*) droid_get_component( _game, _entity, MOVABLE_ID );
			c_position* _position = (c_position*) droid_get_component( _game, _entity, POSITION_ID );

			_movable->vx += _movable->vx_const;
			_movable->vy += _movable->vy_const;

			_position->x += _movable->vx * (float) _game->frame_delta;
			_position->y += _movable->vy * (float) _game->frame_delta;

			_movable->vx = 0.0f;
			_movable->vy = 0.0f;
		}
	}
}

void SYSTEM_Emitter( droid_game* _game, droid_system* _system )
{
	int i=0;
	droid_entity* entities = droid_get_entities( _game );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_emitter* _emitter   = (c_emitter*) droid_get_component( _game, _entity, EMITTER_ID );
			_emitter->timer += (float) _game->frame_delta;

			if ( _emitter->enabled && _emitter->timer >= _emitter->interval )
			{
				droid_entity_msg _msg;
				_msg.type = DROID_ENTITY_MSG_EMIT;

				droid_send_message_to_entity( _game, _entity, &_msg );

				_emitter->timer = 0.0f;
			}
		}
	}
}

void SYSTEM_Bullet( droid_game* _game, droid_system* _system )
{
	int i=0;
	droid_entity* entities = droid_get_entities( _game );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_bullet* _bullet   = (c_bullet*) droid_get_component( _game, _entity, BULLET_ID );

			_bullet->timer += (float) _game->frame_delta;

			if ( _bullet->timer >= _bullet->duration )
			{
				droid_entity_msg _msg;
				_msg.type = DROID_ENTITY_MSG_DESTROY_BULLET;
				
				droid_send_message_to_entity( _game, _entity, &_msg );
			}
		}
	}
}

int TestAABBAABB(c_collider_volume a, c_collider_volume b)
{
	// Exit with no intersection if separated along an axis
	if (a.aabb.max[0] < b.aabb.min[0] || a.aabb.min[0] > b.aabb.max[0]) return 0;
	if (a.aabb.max[1] < b.aabb.min[1] || a.aabb.min[1] > b.aabb.max[1]) return 0;
	// Overlapping on all axes means AABBs are intersecting
	return 1;
}

int IntersectMovingAABBAABB( c_collider_volume a, c_collider_volume b, float va[2], float vb[2], float* tfirst, float *tlast )
{
	// Exit early if a and b initially overlapping
	if (TestAABBAABB(a, b)) {
		*tfirst = 0.0f;
		*tlast = 0.0f;
		return 1;
	}

	return 0;
}

typedef struct
{
	droid_entity* a;
	droid_entity* b;
} c_collider_collision_pair;

void SYSTEM_Collider( droid_game* _game, droid_system* _system )
{
	int i=0,j=0;
	int num_collidables          = 0;
	int num_collidables_pair     = 0;
	droid_entity* entities       = droid_get_entities( _game );
	droid_cache* collision_pairs = droid_cache_create( 4, sizeof( c_collider_collision_pair ) );
	droid_entity* collidables[NUM_ENTITIES];

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = droid_get_entity( _game, i );

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			collidables[num_collidables++] = _entity;
		}
	}

	for( i=0; i < num_collidables; i++ )
	{
		droid_entity* _entity_a = collidables[i];

		for( j=i+1; j < num_collidables; j++ )
		{
			droid_entity* _entity_b = collidables[j];
			c_collider_collision_pair* _pair = 0;

			_pair = (c_collider_collision_pair*) droid_cache_get( collision_pairs, num_collidables_pair++ );

			_pair->a = _entity_a;
			_pair->b = _entity_b;
		}
	}

	for( i=0; i < (int) droid_cache_size( collision_pairs ); i++ )
	{
		c_collider_collision_pair* _pair = droid_cache_get( collision_pairs, i );
		droid_entity* a = _pair->a;
		droid_entity* b = _pair->b;

		c_collider* ca = (c_collider*) droid_get_component( _game, a, COLLIDER_ID );
		c_collider* cb = (c_collider*) droid_get_component( _game, b, COLLIDER_ID );

		c_movable* ma = (c_movable*) droid_get_component( _game, a, MOVABLE_ID );
		c_movable* mb = (c_movable*) droid_get_component( _game, b, MOVABLE_ID );

		c_position* pa = (c_position*) droid_get_component( _game, a, POSITION_ID );
		c_position* pb = (c_position*) droid_get_component( _game, b, POSITION_ID );

		if ( ca->type == DROID_COLLIDER_AABB && cb->type == DROID_COLLIDER_AABB )
		{
			float mva[2],mvb[2];
			float tfirst,tlast;

			c_collider_volume aabb_a;
			c_collider_volume aabb_b;

			aabb_a.aabb.min[0] = pa->x + ca->volume.aabb.min[0];
			aabb_a.aabb.min[1] = pa->y + ca->volume.aabb.min[1];

			aabb_a.aabb.max[0] = pa->x + ca->volume.aabb.max[0];
			aabb_a.aabb.max[1] = pa->y + ca->volume.aabb.max[1];

			aabb_b.aabb.min[0] = pb->x + cb->volume.aabb.min[0];
			aabb_b.aabb.min[1] = pb->y + cb->volume.aabb.min[1];

			aabb_b.aabb.max[0] = pb->x + cb->volume.aabb.max[0];
			aabb_b.aabb.max[1] = pb->y + cb->volume.aabb.max[1];

			mva[0] = ma->vx;
			mva[1] = ma->vy;

			mvb[0] = ma->vx;
			mvb[1] = ma->vy;

			if (IntersectMovingAABBAABB( aabb_a, aabb_b, mva, mvb, &tfirst, &tlast ))
			{
				droid_entity_msg _msg;
				_msg.type     = DROID_ENTITY_MSG_COLLISION;
				_msg.userdata = b;
				
				droid_send_message_to_entity( app->game, a, &_msg );

				_msg.userdata = a;
				droid_send_message_to_entity( app->game, b, &_msg );
			}

		} else {
			// unsupported collision type
			assert(0);
		}
	}	
}