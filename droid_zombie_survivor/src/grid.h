#ifndef __GRID_H__
#define __GRID_H__

#include "droid.h"

#define CELL_TYPE_NONE -1

typedef struct 
{
	int type;
	int passable;
} t_cell;

typedef struct
{
	droid_quadbatch* qb;

	int c_width;
	int c_height;

	float width;
	float height;

	t_cell* cells;
	t_cell* neighbour_list[8];

} t_grid;

t_grid* grid_create( int _cw, int _ch, float _width, float _height );
void grid_destroy( t_grid* _grid );

t_cell* grid_get_cell( t_grid* _grid, int _cx, int _cy );
int grid_in_bounds( t_grid* _grid, int _cx, int _cy );
int grid_passable( t_grid* _grid, int _cx, int _cy );

t_cell** grid_get_neighbours( t_grid* _grid, int _cx, int _cy );

#endif