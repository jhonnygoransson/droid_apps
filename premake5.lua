local host_def = "PLATFORM_UNSUPPORTED"
local host_os  = os.get()

if host_os == "windows" then
	host_def = "PLATFORM_WINDOWS"
elseif host_os == "linux" then
	host_def = "PLATFORM_LINUX"
elseif host_os == "macosx" then
	host_def = "PLATFORM_OSX"
end

if ( buildroot == nil ) then
	buildroot = "build"
end

if ( includeroot == nil ) then
	includeroot = ""
end

solution "droid_apps"
	configurations { "Debug","Release" }
	location(buildroot)
	language "C"

	configuration "Debug"
		defines { "DEBUG", host_def }
		flags { "Symbols" }

	configuration "Release"
		defines { "NDEBUG", host_def }
		flags { "Optimize" }

	require("droid_pong/premake5")
	require("droid_survivor/premake5")
	require("droid_zombie_survivor/premake5")