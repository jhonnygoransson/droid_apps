attribute vec3 a_position;
attribute vec2 a_uv0;

uniform mat4 u_transform;

varying vec2 v_uv;

void main()
{
	v_uv                = a_uv0;	
	vec4 world_position = u_transform * vec4(a_position,1.0);
	gl_Position         = vec4( world_position.xyz, 1.0);
}