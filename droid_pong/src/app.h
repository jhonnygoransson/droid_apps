#ifndef __APP_H__
#define __APP_H__

#include "droid.h"

#define NUM_ENTITIES 100

typedef struct
{
	droid_instance* droid;
	droid_game* 	game;

	droid_shader*   shader;
	droid_camera*   camera;
} t_app;

int app_create();
int app_run();

t_app* app;
unsigned int RENDERABLE_ID;
unsigned int POSITION_ID;

#endif // __APP_H__