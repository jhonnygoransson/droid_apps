#include <malloc.h>

#include "app.h"
#include "components.h"

extern void SYSTEM_Render( droid_game*, droid_system* );

void handler_player( droid_game* _game, droid_entity* _entity, droid_entity_msg* _msg )
{
	DROID_DEBUG( "Entity %d received msg type %d", _entity->id, _msg->type );

	if ( _msg->type == DROID_ENTITY_MSG_COMPONENT_ADDED )
	{
		unsigned int _c = *((unsigned int*) _msg->userdata);

		DROID_DEBUG( "  Added Component %d", _c);
	}

	else if ( _msg->type == DROID_ENTITY_MSG_COMPONENT_REMOVED )
	{
		unsigned int _c = *((unsigned int*) _msg->userdata);

		DROID_DEBUG( "  Removed Component %d", _c);
	}
}

void create_player( droid_game* _game, droid_camera* _camera )
{
	droid_entity* _player = droid_new_entity( _game, handler_player );

	if (!_player) {
		DROID_DEBUG("Unable to create player");
	}

	droid_enable_components( _game, _player, RENDERABLE_ID | POSITION_ID );
	{
		c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _player, RENDERABLE_ID );
		_renderable->camera = _camera;
	}
}

void on_keyboard( droid_context* _ctx, droid_event_msg _msg )
{
	droid_input_keyboard_event* _event_data = 0;

	if(!_msg.eventdata) return;

	_event_data = (droid_input_keyboard_event*) _msg.eventdata;

	// exit on esc-press
	if ( _event_data->button == DROID_KEY_ESCAPE && _event_data->type == DROID_KEYBOARD_RELEASED )
	{
		droid_exit();	
	} 
}

void on_init( droid_context* _ctx, droid_event_msg _msg )
{
	droid_add_resource_location("pong_assets");

	// get droid instance
	app->droid = droid_get();

	// load assets : shader
	app->shader = droid_load_shader( "shader.vs", "shader.fs" );

	// create game camera
	app->camera = droid_create_camera_orthographic( 
		(float) -app->droid->cfg.window_width/2.0f, 
		(float) app->droid->cfg.window_width/2.0f, 
		(float) -app->droid->cfg.window_height/2.0f, 
		(float) app->droid->cfg.window_height/2.0f, 
		0.0f, 1.0f );

	{
		// add components
		RENDERABLE_ID = droid_add_component( app->game, sizeof(c_renderable) );
		POSITION_ID   = droid_add_component( app->game, sizeof(c_position) );

		// add systems
		droid_add_system( app->game, SYSTEM_Render, RENDERABLE_ID | POSITION_ID );

		// create entities
		droid_allocate_entities( app->game, NUM_ENTITIES );
		
		create_player( app->game, app->camera );
	}
}

void on_render( droid_context* _ctx, droid_event_msg _msg )
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT );

	droid_run_systems( app->game );
}

int app_create()
{
	if ( app ) return 1;

	app = (t_app*) malloc( sizeof(app) );

	app->droid  = 0;
	app->game   = 0;
	app->shader = 0;
	app->camera = 0;

	return (app > 0);
}

int app_run()
{
	droid_config cfg;

	cfg.window_width  		= 512;
	cfg.window_height 		= 512;
	cfg.window_fullscreen 	= 0;
	cfg.window_title  		= "Pong 1.0";

	if(!droid_create( cfg )) return 0;

	app->game = droid_create_game();

	droid_on_event( DROID_EVENT_INIT, &on_init );
	droid_on_event( DROID_EVENT_RENDER, &on_render );
	droid_on_event( DROID_EVENT_INPUT_KEYBOARD, &on_keyboard );

	droid_run();
	droid_destroy();

	// DROID_FREE_PTR(app);

	return 1;
}
