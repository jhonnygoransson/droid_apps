#ifndef __COMPONENTS_H__
#define __COMPONENTS_H__

#include "droid_game/droid_camera.h"

typedef struct 
{
	droid_camera* camera;
} c_renderable;

typedef struct 
{
	float x;
	float y;
} c_position;

#endif
