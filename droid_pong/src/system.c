#include "app.h"
#include "components.h"

extern t_app* app;
extern unsigned int RENDERABLE_ID;
extern unsigned int POSITION_ID;

void SYSTEM_Render( droid_game* _game, droid_system* _system )
{
	unsigned int i=0;
	droid_entity* entities = droid_get_entities( _game );

	for( i; i < NUM_ENTITIES; i++ )
	{
		droid_entity* _entity = entities+i;

		if( _entity->id != DROID_ENTITY_UNUSED && DROID_HAS_FLAGS(_entity->components,_system->mask))
		{
			c_renderable* _renderable = (c_renderable*) droid_get_component( _game, _entity, RENDERABLE_ID );
			c_position* _position     = (c_position*) droid_get_component( _game, _entity, POSITION_ID );
		}
	}

	/*
	unsigned long* entities = droid_get_entities( app->game );
	unsigned int i=0;

	droid_quadbatch_bind( app->qb, app->shader );

	for(i;i<NUM_ENTITIES;i++)
	{
		unsigned long entity = *(entities+i);

		if ( DROID_HAS_FLAGS(entity,mask) )
		{
			c_renderable* _renderable = (c_renderable*) droid_get_component( app->game, i, renderable_id );
			c_position* _position     = (c_position*) droid_get_component( app->game, i, position_id );

			droid_mat4x4 _transform;
			droid_mat4x4_identity(_transform);
			droid_mat4x4_translate( _transform, _position->x, _position->y, 0.0f);
			droid_mat4x4_scale_aniso( _transform, _transform, _renderable->scale_x, _renderable->scale_y, 1.0 );

			droid_bind_shader_uniform_matrix4fv( app->shader, "u_transform", (float*) &_transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_camera_transform", (float*) &app->camera->transform, 0 );
			droid_bind_shader_uniform_matrix4fv( app->shader, "u_projection", (float*) &app->camera->projection, 0 );
			droid_bind_shader_uniform_i( app->shader, "u_material", _renderable->material );
			droid_bind_shader_uniform_3f( app->shader, "u_color", _renderable->color[0], _renderable->color[1], _renderable->color[2] );

			// DO RENDER HERE
			droid_quadbatch_draw();
		}
	}

	droid_quadbatch_unbind();
	*/
	
}